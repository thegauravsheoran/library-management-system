package com.example.Library.Management.System.service;

import com.example.Library.Management.System.entity.Book;
import com.example.Library.Management.System.repository.BookRepository;
import jakarta.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book addBook(Book book) {
        return bookRepository.save(book);
    }

    public Optional<Book> getBookById(Long id) {
        return bookRepository.findById(id);
    }

    public List<Book> getAllBooks() {
        List<Book> books = bookRepository.findAll();
        if (books.isEmpty()) {
            throw new EntityNotFoundException("No books found");
        }
        return books;
    }

    public Book updateBook(Long id, Book updatedBook) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (optionalBook.isPresent()) {
            Book book = optionalBook.get();
            book.setName(updatedBook.getName());
            return bookRepository.save(book);
        }
        throw new EntityNotFoundException("No books found to update");
    }

    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    public List<Book> getBooksByCategory(String category) {
        List<Book> booksByCategoryName = bookRepository.findByCategoriesName(category);
        if (booksByCategoryName.isEmpty()) {
            throw new EntityNotFoundException("Books not present for this Category");
        } else {
            return booksByCategoryName;
        }
    }

    public List<Book> getBooksBySection(String section) {
        List<Book> booksBySectionName = bookRepository.findBySectionsName(section);
        if (booksBySectionName.isEmpty()) {
            throw new EntityNotFoundException("Books not present for this Section");
        } else {
            return booksBySectionName;
        }
    }

    public List<Book> getBooksByCategoryAndSection(String category, String section) {
        List<Book> booksBySectionAndCategoryName = bookRepository.findByCategoriesNameAndSectionsName(category, section);
        if (booksBySectionAndCategoryName.isEmpty()) {
            throw new EntityNotFoundException("Books not present for this Category and section");
        } else {
            return booksBySectionAndCategoryName;
        }
    }
}
