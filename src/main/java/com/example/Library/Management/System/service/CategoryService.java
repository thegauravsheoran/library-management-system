package com.example.Library.Management.System.service;

import com.example.Library.Management.System.entity.Category;
import com.example.Library.Management.System.repository.CategoryRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@Data
@Component
public class CategoryService {

    @Autowired
    public CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category addCategory(Category category) {
        return categoryRepository.save(category);
    }

    public Optional<Category> getCategoryById(Long id) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (optionalCategory.isEmpty()) {
            throw new EntityNotFoundException("Category is empty");
        } else {
            return optionalCategory;
        }
    }

    public List<Category> getAllCategories() {
        List<Category> category = categoryRepository.findAll();
        if (category.isEmpty()) {
            throw new EntityNotFoundException("Category is Empty");
        } else {
            return category;
        }
    }

    public Category updateCategory(Long id, Category updatedCategory) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (optionalCategory.isPresent()) {
            Category category = optionalCategory.get();
            category.setName(updatedCategory.getName());
            return categoryRepository.save(category);
        }
        throw new EntityNotFoundException("Category is empty cannot update");
    }

    public void deleteCategory(Long id) {
        categoryRepository.deleteById(id);
    }
}
