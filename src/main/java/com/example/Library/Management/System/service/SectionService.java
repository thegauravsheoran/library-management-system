package com.example.Library.Management.System.service;

import com.example.Library.Management.System.entity.Section;
import com.example.Library.Management.System.repository.SectionRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SectionService {

    @Autowired
    private SectionRepository sectionRepository;

    public Section addSection(Section section) {
        try {
            return sectionRepository.save(section);
        } catch (Exception e) {
            throw new RuntimeException("Error adding section: " + e.getMessage(), e);
        }
    }

    public Optional<Section> getSectionById(Long id) {
        Optional<Section> optionalSection = sectionRepository.findById(id);
        if (optionalSection.isEmpty()) {
            throw new EntityNotFoundException("Section is empty");
        } else {
            return optionalSection;
        }
    }

    public List<Section> getAllSections() {
        List<Section> section = sectionRepository.findAll();
        if (section.isEmpty()) {
            throw new EntityNotFoundException("Section is Empty");
        } else {
            return section;
        }
    }

    public Section updateSection(Long id, Section updatedSection) {
        Optional<Section> optionalSection = sectionRepository.findById(id);
        if (optionalSection.isPresent()) {
            Section section = optionalSection.get();
            section.setName(updatedSection.getName());
            return sectionRepository.save(section);
        }
        throw new EntityNotFoundException("Section is Empty cannot update");
    }

    public void deleteSection(Long id) {
        sectionRepository.deleteById(id);
    }


}
