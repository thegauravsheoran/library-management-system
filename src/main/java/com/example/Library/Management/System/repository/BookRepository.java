package com.example.Library.Management.System.repository;

import com.example.Library.Management.System.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    List<Book> findByCategoriesName(String category);

    List<Book> findBySectionsName(String section);

    List<Book> findByCategoriesNameAndSectionsName(String category, String section);

}
