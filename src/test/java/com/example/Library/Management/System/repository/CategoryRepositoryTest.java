package com.example.Library.Management.System.repository;

import com.example.Library.Management.System.entity.Category;
import com.example.Library.Management.System.service.CategoryService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class CategoryRepositoryTest {

    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private CategoryService categoryService;

    @Test
    public void testFindCategoryById() {
        Long categoryId = 1L;
        Category expectedCategory = new Category(categoryId, "Test Category");
        when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(expectedCategory));
        Optional<Category> actualCategory = categoryService.getCategoryById(categoryId);
        assertThat(actualCategory).isEqualTo(Optional.of(expectedCategory));
    }
}
