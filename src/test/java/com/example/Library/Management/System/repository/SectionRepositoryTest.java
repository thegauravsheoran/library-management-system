package com.example.Library.Management.System.repository;


import com.example.Library.Management.System.entity.Category;
import com.example.Library.Management.System.entity.Section;
import com.example.Library.Management.System.service.SectionService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
public class SectionRepositoryTest {
    @Mock
    private SectionRepository sectionRepository;
    @InjectMocks
    private SectionService sectionService;


    @Test
    void testFindSectionById(){
        Section expectedSection = new Section(1L, "Test Section");
        when(sectionRepository.findById(1L)).thenReturn(Optional.of(expectedSection));
        Optional<Section> actualSection = sectionService.getSectionById(1L);
        assertThat(actualSection).isEqualTo(Optional.of(expectedSection));
    }

}
