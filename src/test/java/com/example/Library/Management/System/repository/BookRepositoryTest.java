package com.example.Library.Management.System.repository;

import com.example.Library.Management.System.entity.Book;
import com.example.Library.Management.System.service.BookService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class BookRepositoryTest {

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookService bookService;

    @Test
    public void testFindByCategoriesName() {
        String category = "Science Fiction";
        List<Book> expectedBooks = Arrays.asList(
                new Book(1L, "Book 1", category),
                new Book(2L, "Book 2", category)
        );

        when(bookRepository.findByCategoriesName(category)).thenReturn(expectedBooks);

        List<Book> actualBooks = bookService.getBooksByCategory(category);

        assertThat(actualBooks).isEqualTo(expectedBooks);
    }

    @Test
    public void testFindBySectionsName() {
        String section = "Fantasy";
        List<Book> expectedBooks = Arrays.asList(
                new Book(3L, "Book 3", "Science Fiction"),
                new Book(4L, "Book 4", "Mystery"),
                new Book(5L, "Book 5", section)
        );
        when(bookRepository.findBySectionsName(section)).thenReturn(expectedBooks);

        List<Book> actualBooks = bookService.getBooksBySection(section);

        assertThat(actualBooks).isEqualTo(expectedBooks);
    }

    @Test
    public void testFindByCategoriesNameAndSectionsName() {
        String category = "Science Fiction";
        String section = "Fantasy";
        List<Book> expectedBooks = Arrays.asList(
                new Book(6L, "Book 6", category, section),
                new Book(7L, "Book 7", category, section)
        );

        when(bookRepository.findByCategoriesNameAndSectionsName(category, section)).thenReturn(expectedBooks);

        List<Book> actualBooks = bookService.getBooksByCategoryAndSection(category, section);

        assertThat(actualBooks).isEqualTo(expectedBooks);
    }
}

