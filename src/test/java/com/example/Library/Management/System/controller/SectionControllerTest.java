package com.example.Library.Management.System.controller;

import com.example.Library.Management.System.entity.Section;
import com.example.Library.Management.System.service.SectionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class SectionControllerTest {
    @Mock
    private SectionService sectionService;

    private MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        mockMvc = standaloneSetup(new SectionController(sectionService)).build();
    }

    @Test
    public void testAddSection() throws Exception{
        Section section= new Section();
        section.setId(1L);
        when(sectionService.addSection(section)).thenReturn(section);
        mockMvc.perform(post("/sections/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(section)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));
    }

    @Test
    public void testGetsectionById() throws Exception {
        Section section= new Section();
        section.setId(1L);
        Long newsectionId = 1L;
        when(sectionService.getSectionById(newsectionId)).thenReturn(Optional.of(section));
        mockMvc.perform(get("/sections/get/{id}", newsectionId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(section)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(newsectionId));
    }


    @Test
    public void testGetsections() throws Exception {
        Section section= new Section();
        section.setId(1L);
        when(sectionService.getAllSections()).thenReturn(Collections.singletonList(section));
        mockMvc.perform(get("/sections/get")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").exists());
    }

    @Test
    void testUpdatesection() throws Exception {
        Long sectionId = 1L;
        Section updatedsection = new Section();
        updatedsection.setId(1L);
        updatedsection.setName("Updated section"); // Set the name to the updated value

        when(sectionService.updateSection(eq(sectionId), any(Section.class))).thenReturn(updatedsection);

        mockMvc.perform(put("/sections/update/{id}", sectionId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedsection))) // Serialize updatedsection to JSON
                .andExpect(status().isOk()) // Expecting a successful response
                .andDo(print())
                .andExpect(jsonPath("$.id").value(sectionId)) // Expecting the section ID to be the same
                .andExpect(jsonPath("$.name").value("Updated section")); // Expecting the name to be updated
    }


    @Test
    void testDeleteBook() throws Exception {
        Long bookId = 1L;
        doNothing().when(sectionService).deleteSection(bookId);
        mockMvc.perform(delete("/sections/delete/{id}", bookId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }



}
