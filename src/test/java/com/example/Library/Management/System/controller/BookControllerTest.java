package com.example.Library.Management.System.controller;


import com.example.Library.Management.System.entity.Book;
import com.example.Library.Management.System.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@SpringBootTest
public class BookControllerTest {

    @Mock
    private BookService bookService;

    private MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        mockMvc = standaloneSetup(new BookController(bookService)).build();
    }


    @Test
    public void testAddBook() throws Exception {
        Book book = new Book();
        book.setId(1L);
        book.setName("Your Book Name");
        Long newBookId = 1L;
        when(bookService.addBook(book)).thenReturn(book);

        mockMvc.perform(post("/book/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(book)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(newBookId));
    }


    @Test
    public void testGetBookById() throws Exception {
        Long bookId = 1L;
        Book book = new Book();
        book.setId(1L);
        when(bookService.getBookById(bookId)).thenReturn(Optional.of(book));

        mockMvc.perform(get("/book/get/{id}", bookId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(bookId));
    }

    @Test
    public void testGetAllBooks() throws Exception {
        Book book = new Book();
        book.setId(1L);
        when(bookService.getAllBooks()).thenReturn(Collections.singletonList(book));

        mockMvc.perform(get("/book/get")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").exists());
    }

    @Test
    public void testListBooks() throws Exception {
        String category = "Fiction";
        String section = "Science";
        Book book = new Book();
        book.setId(1L);
        when(bookService.getBooksByCategoryAndSection(category, section)).thenReturn(Collections.singletonList(book));

        mockMvc.perform(get("/book/list")
                        .param("category", category)
                        .param("section", section)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").exists());
    }

    @Test
    void testUpdateBook() throws Exception {
        Long bookId = 1L;
        Book updatedBook = new Book();
        updatedBook.setId(1L);
        updatedBook.setName("Updated Book"); // Set the name to the updated value

        when(bookService.updateBook(eq(bookId), any(Book.class))).thenReturn(updatedBook);

        mockMvc.perform(put("/book/update/{id}", bookId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedBook))) // Serialize updatedBook to JSON
                .andExpect(status().isOk()) // Expecting a successful response
                .andDo(print())
                .andExpect(jsonPath("$.id").value(bookId)) // Expecting the book ID to be the same
                .andExpect(jsonPath("$.name").value("Updated Book")); // Expecting the name to be updated
    }


    @Test
    void testDeleteBook() throws Exception {
        Long bookId = 1L;

        doNothing().when(bookService).deleteBook(bookId);

        mockMvc.perform(delete("/book/delete/{id}", bookId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()); // Expecting a successful response (HTTP 200)

    }

}
