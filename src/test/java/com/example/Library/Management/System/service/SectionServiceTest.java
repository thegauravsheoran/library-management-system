package com.example.Library.Management.System.service;


import com.example.Library.Management.System.entity.Category;
import com.example.Library.Management.System.entity.Section;
import com.example.Library.Management.System.repository.CategoryRepository;
import com.example.Library.Management.System.repository.SectionRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@SpringBootTest
public class SectionServiceTest {

    @Autowired
    private SectionService sectionService;
    @MockBean
    private SectionRepository sectionRepository;

    @Test
    void testGetSectionById() {
        assertThrows(EntityNotFoundException.class, () -> {
            sectionService.getSectionById(1L);
        });
        verify(sectionRepository, times(1)).findById(1L);
    }

    @Test
    void testGetAllSections() {
        when(sectionRepository.findAll()).thenReturn(List.of(new Section()));

        List<Section> sections = sectionService.getAllSections();

        assertNotNull(sections);
        Mockito.verify(sectionRepository, times(1)).findAll();
    }

    @Test
    void testUpdateCategory() {
        Long categoryId = 1L;
        Category updatedcategory = new Category();
        updatedcategory.setName("Updated category Name");

        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        CategoryService categoryService = new CategoryService(categoryRepository);

        Category existingcategory = new Category();
        Optional<Category> optionalcategory = Optional.of(existingcategory);
        when(categoryRepository.findById(categoryId)).thenReturn(optionalcategory);
        when(categoryRepository.save(existingcategory)).thenReturn(existingcategory);

        Category result = categoryService.updateCategory(categoryId, updatedcategory);

        assertNotNull(result);
        assertEquals(updatedcategory.getName(), result.getName());
        verify(categoryRepository, times(1)).findById(categoryId);
        verify(categoryRepository, times(1)).save(existingcategory);
    }

    @Test
    void testDeleteCategory() {
        sectionService.deleteSection(1L);
        verify(sectionRepository, times(1)).deleteById(1L);
    }

}
