package com.example.Library.Management.System.service;


import com.example.Library.Management.System.entity.Book;
import com.example.Library.Management.System.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@SpringBootTest
public class BookServiceTest {

    @Autowired
    private BookService bookService;

    @MockBean
    private BookRepository bookRepository;

    @Test
    public void testAddBookSuccess() {
        Book book = new Book();
        book.setName("Sample Book");
        when(bookRepository.save(book)).thenReturn(book);
        Book savedBook = bookService.addBook(book);
        assertEquals(book, savedBook);
    }

    @Test
    void testGetBookById() {
        bookService.getBookById(1L);
        verify(bookRepository, times(1)).findById(1L);
    }

    @Test
    void testGetAllBooks() {
        when(bookRepository.findAll()).thenReturn(List.of(new Book()));

        Iterable<Book> books = bookService.getAllBooks();

        assertNotNull(books);
        verify(bookRepository, times(1)).findAll();
    }

    @Test
    void testUpdateBook() {
        Long bookId = 1L;
        Book updatedBook = new Book();
        updatedBook.setName("Updated Book Name");
        BookRepository bookRepository = mock(BookRepository.class);
        BookService bookService = new BookService(bookRepository);

        Book existingBook = new Book();
        Optional<Book> optionalBook = Optional.of(existingBook);
        when(bookRepository.findById(bookId)).thenReturn(optionalBook);
        when(bookRepository.save(existingBook)).thenReturn(existingBook);

        Book result = bookService.updateBook(bookId, updatedBook);

        assertNotNull(result);
        assertEquals(updatedBook.getName(), result.getName());
        verify(bookRepository, times(1)).findById(bookId);
        verify(bookRepository, times(1)).save(existingBook);
    }

    @Test
    void testDeleteBook() {
        bookService.deleteBook(1L);
        verify(bookRepository, times(1)).deleteById(1L);
    }

}
