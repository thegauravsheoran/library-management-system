package com.example.Library.Management.System.controller;

import com.example.Library.Management.System.entity.Category;
import com.example.Library.Management.System.service.CategoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CategoryControllerTest {
    @Mock
    private CategoryService categoryService;

    private MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        mockMvc = standaloneSetup(new CategoryController(categoryService)).build();
    }

    @Test
    public void testAddCategories() throws Exception {
        Category category= new Category();
        category.setId(1L);
        when(categoryService.addCategory(category)).thenReturn(category);
        mockMvc.perform(post("/categories/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(category)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L));
    }

    @Test
    public void testGetCategoryById() throws Exception {
        Category category= new Category();
        category.setId(1L);
        Long newcategoryId = 1L;
        when(categoryService.getCategoryById(newcategoryId)).thenReturn(Optional.of(category));
        mockMvc.perform(get("/categories/get/{id}", newcategoryId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(category)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(newcategoryId));
    }


    @Test
    public void testGetCategories() throws Exception {
        Category category= new Category();
        category.setId(1L);
        when(categoryService.getAllCategories()).thenReturn(Collections.singletonList(category));
        mockMvc.perform(get("/categories/get")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").exists());
    }

    @Test
    void testUpdatecategory() throws Exception {
        Long categoryId = 1L;
        Category updatedcategory = new Category();
        updatedcategory.setId(1L);
        updatedcategory.setName("Updated category"); // Set the name to the updated value

        when(categoryService.updateCategory(eq(categoryId), any(Category.class))).thenReturn(updatedcategory);

        mockMvc.perform(put("/categories/update/{id}", categoryId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedcategory))) // Serialize updatedcategory to JSON
                .andExpect(status().isOk()) // Expecting a successful response
                .andDo(print())
                .andExpect(jsonPath("$.id").value(categoryId)) // Expecting the category ID to be the same
                .andExpect(jsonPath("$.name").value("Updated category")); // Expecting the name to be updated
    }


    @Test
    void testDeleteBook() throws Exception {
        Long bookId = 1L;
        doNothing().when(categoryService).deleteCategory(bookId);
        mockMvc.perform(delete("/categories/delete/{id}", bookId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


}
